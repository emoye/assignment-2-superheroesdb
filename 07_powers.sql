INSERT INTO SuperPower
           ([PowerName]
           ,[PowerDescription])
     VALUES
           ('Super speed'
           ,'Run really fast'),
		   ('Super strength'
           ,'Really really strong'),
		   ('Teleportation'
           ,'Teleport anywhere'),
		   ('Time travel'
           ,'Travel through time')

INSERT INTO SuperheroPowerLink
           ([HeroID]
           ,[PowerID])
     VALUES
           (1
           ,1),
		   (1
		   ,2),
		   (2
		   ,1),
		   (3
		   ,4)



